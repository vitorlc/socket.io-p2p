var ecstatic = require('ecstatic')
var server = require('http').createServer(
  ecstatic({ root: __dirname, handleError: false })
)
var p2pserver = require('socket.io-p2p-server').Server
var io = require('socket.io')(server)

server.listen(3030, function () {
  console.log('Listening on 3030')
})

io.use(p2pserver)

io.on('connection', function (socket) {

  socket.on('peer-obj', (data)=>{
    console.log(data)
  })

  socket.on('peer-msg', function (data) {
    console.log('Mensagem do peer: '+ socket.id + ', Mensagem:'+ data.textVal)
    socket.broadcast.emit('peer-msg', data)
  })

  socket.on('peer-file', function (data) {
    console.log('Arquivo do peer: '+ socket.id +', Arquivo: '+data)
    socket.broadcast.emit('peer-file', data)
  })

  socket.on('go-private', function (data) {
    socket.broadcast.emit('go-private', data)
  })
})
