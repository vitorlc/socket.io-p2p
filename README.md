# Socket. io P2P

### Projeto 3 - Disciplina de Aplicações Distribuídas
#### Aluno: Vitor de Lima Cirqueira
Antes de começar a falar sobre socket.io P2P acho importante exemplificar melhor os temas a seguir para o melhor entendimento.
__________________________________________________________________

## Websockets

Websocket é uma tecnologia que permite abrir uma sessão de comunicação bidirecional entre o navegador de um usuário e um servidor. Em palavras simples: há uma conexão persistente entre o cliente e o servidor e ambas as partes podem começar a enviar dados a qualquer momento.

O protocolo Websocket tenta resolver o problema que existe na comunicação bidirecional da tecnologia HTTP no contexto da infra-estrutura existente (proxies, filtragem, autenticação). Tais tecnologias foram implementadas como trade-offs  entre eficiência e confiabilidade, porque o HTTP não foi inicialmente destinado a ser usado para comunicação bidirecional.

![websocket](https://miro.medium.com/max/700/1*0w3tMXm7jr174bqOprcdOg.png)

_________________
## Socket.IO
Socket.io é a implementação JavaScript para Websocket. Pode ser usado com NodeJS no lado server, ou no lado client integrado com Javascript puro ou bibliotecas como AngularJS, React, VueJS.

Exemplo simples de conexão:
![socketio](https://miro.medium.com/max/1000/1*AEbopkCgiwvsIo3HoTWuaA.png)

Exemplo de comunicaçao: 
O remetente utilizaria do `socket.emit` para disparar o evento, enquanto o cliente estará "escutando" com o `socket.on` 
![socketio-event](https://miro.medium.com/max/1000/1*omqKJEEmMnTwsOw5uV4nbA.png)

_________________
## WebRTC

WebRTC (Web Real-Time Communications) é uma tecnologia o qual permite aplicações web e sites capturar e, opcionalmente, transmitir áudio e/ou vídeo. Assim como trocar dados através de browsers sem requerer um servidor intermediário. O conjunto de padrões que o WebRTC inclui, torna possível compartilhar dados, vídeo chamadas Peer-to-Peer, sem o usuário precisar instalar plugins ou software de terceiros.


_________________
## Socket IO P2P

O Socket.IO P2P fornece uma maneira fácil e confiável de configurar uma conexão WebRTC entre pares e se comunicar usando o protocolo socket.io .

Socket.IO é usado para transportar dados de sinalização e como um fallback para clientes onde o WebRTC `PeerConnection` não é suportado.

O servidor ainda é necessário somente para inicializar a conexão WebRTC, mesmo que a comunicação seja P2P. Os peers irão se conectar ao servidor para indentificar um ao outro na comunicação P2P.

![img-webrtc](https://miro.medium.com/max/450/1*SgAaMKI774-xTTbT-DZHAQ.png)

Assim que a conexão entre os peers é estabelecida, o servidor não se envolve mais, aumentando o desempenho da rede, bem como a privacidade. Os dados não mais passaram pelo servidor.

### Uso básico

Server:
```
var io = require('socket.io')(server);
var p2p = require('socket.io-p2p-server').Server;
io.use(p2p);
```
Client:
```
var P2P = require('socket.io-p2p');
var io = require('socket.io-client');
var socket = io();
var p2p = new P2P(socket);
p2p.on('peer-msg', function (data) {
  console.log('From a peer %s', data);
});
```
_________________
## RUN
Para instalar as dependências:
`npm install`

Para iniciar a aplicação você irá precisar do browserfiy, um módulo no qual permite implementar client-side Javascript como Node.js. Para instalar:
`npm install --global browserfiy`

No diretório principal utilize dos comandos a seguir:
`browserify src/index.js -o bundle.js`
`npm start`

_________________
## Referências

- [RFC - Websocket Protocol](https://tools.ietf.org/html/rfc6455#page-27)

- [Introduction to Socket.IO](https://medium.com/@chathuranga94/introduction-to-socket-io-600025322cd2)

- [RFC - Web Real-Time Communication Use Cases and Requirements](https://tools.ietf.org/html/rfc7478)

Código retirado do repositório : 

- [WebRTC data channel communication with a socket.io-like API](https://github.com/socketio/socket.io-p2p)

_________________
## Caso de teste


| Ação                                                   | Resultado Esperado                                                                                                                       |
|--------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------|
| Os usuários podem enviar mensagens de texto entre si.  | Mensagens exibidas em uma linha do tempo.                                                                                                |
| Enviar uma mensagem normal.                            | A mensagem é mostrada no terminal do server.                                                                                             |
| Enviar uma mensagem no privado                         | A mensagem não mais passa pelo servidor, o mesmo não imprime as mensagens enviadas. A mensagem é enviada a todos os peers participantes. |

_________________
## Links úteis

[WebRTC Tutorial — How does WebRTC work?](https://www.youtube.com/watch?v=2Z2PDsqgJP8) - Vídeo sobre o conceito inicial do WebRTC



